function I = knn_mi(x, y, nn, dt)
%KNN_MI Calculate the mutual information between systems X and Y using the
% algorithm of Kraskov-Grassberger shown in Estimating Mutual Information
% 2004.
%
% Syntax: I = knn-mi(x, y, nn, dt)
%
% Inputs:
%   x -- Time series from system X
%   y -- Time series from system Y
%   nn -- Number of nearest neighbors
%   dt -- Distance type: 0 inf-norm, 1 euclidean-norm
%
% Outputs:
%   I -- Mutual information
%
% $Date: 2019-04-09
%
% $Author: Juan F. Restrepo, Ph.D.
%
% Copyright (c) 2019, Juan F. Restrepo
% All rights reserved.

% Form z_i data
Dx = distanceMatrix(x(:), dt);
Dy = distanceMatrix(y(:), dt);
Dz = sort(max(Dx, Dy));

N = length(Dx) - 1;

% Find distance to the  k-nearest-neighbor
Eps = Dz(nn + 1, :);
% Obtain nx
nx = sum(Dx < Eps);
% Obtain ny
ny = sum(Dy < Eps);

% Get mutual informatio
I = psi(nn) - mean(psi(nx) + psi(ny)) + psi(N);
