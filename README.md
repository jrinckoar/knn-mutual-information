--------------------------------------------------------------------------------------------------------------------
# Mutual Information Function -- MATLAB CODE
## Juan F. Restrepo
### jrestrepo@ingenieria.uner.edu.ar

*Laboratorio de Señales y Dinámicas no Lineales,  Instituto de Bioingeniería y Bioinformática, CONICET - Universidad
Nacional de Entre Ríos.  Ruta prov 11 km 10 Oro Verde, Entre Ríos, Argentina.*

--------------------------------------------------------------------------------------------------------------------
### Usage

mi_function(x,  y,  tmax,  nn,  dt) Calculates de  mutual  information  function  between  x(t) and y(t+tau),  where
tau=0,1,...,tmax, using the algorithm proposed by Kraskov-Grassberger shown in Estimating Mutual Information 2004.

##### Inputs:
  *   x -- Time series from system X
  *   y -- Time series from system Y
  *   tmax -- Maximum time lag
  *   nn -- Number of nearest neighbors
  *   dt -- Distance type: 0 inf-norm, 1 squared euclidean-norm

##### Outputs:
  *  I -- Mutual information

### Files
1. mi_function.m
2. knn_mi.m
3. distanceMatrix.m
4. example.m

--------------------------------------------------------------------------------------------------------------------
Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>
