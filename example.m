% Test mutual_information function
clear;
clc;

%% Independent variables
N = 10000;
tmax = 20;
nn = 8;
dt = 1;
X = randn(N, 1);
Y = randn(N, 1);
I = 0;

MIF1 = mi_function(X, Y, tmax, nn, dt);
figure();
plot(0:tmax, MIF1);
hold on;
plot([0 tmax], [I, I], 'r'); % Plot Theoretical value
hold off;
xlabel('tau')
ylabel('Mutual Information')

%% Bivariate normal distribution
nn = 10;
dt = 1;
N = 5000;
u1 = -1;
u2 = 1;
sigma1= 1;
sigma2 = 2;
rho = 0.8;
I = -0.5 *log(1-rho^2);

U = repmat([u1;u2], 1, N);
S = [sigma1.^2, rho*sigma1*sigma2; rho*sigma1*sigma2, sigma2^2];
C = chol(S, 'lower');

K = 100;
MIF2 = zeros(K, 1);
parfor i =1: K
    % Generate correlated normal variables
    Z = randn(2, N);
    X = U + C*Z;
    % Calculate mutual information
    MIF2(i) = mi_function(X(1,:), X(2,:), 0, nn, dt);
end
figure();
boxplot(MIF2)
hold on;
plot([0 tmax], [I, I], 'r'); % Plot Theoretical value
hold off;
ylabel('Mutual Information')
