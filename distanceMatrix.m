function D = distanceMatrix(V, dt)
%DISTANCEMATRIX Calculate the distance between m-dimensional vectors.
%
% Syntax: D = distanceMatrix(V, dt)
%
% Inputs:
%   V -- Array of m-dimensional vectors
%   dt -- Distance type: 0 inf-norm, 1 squared-euclidean-norm
%
% Outputs:
%   D -- Distance matrix
%
% $Date: 2019-04-23
%
% $Author: Juan F. Restrepo, Ph.D.
%
% Copyright (c) 2019, Juan F. Restrepo
% All rights reserved.

n = length(V);
D = zeros(n);

for k=0:n - 1
    v1 = V(1:n - k, :)';
    v2 = V(k + 1:n, :)';
    switch dt
        case 0
            z = max(abs(v1 - v2), [], 1)';    % Inf distance.
        case 1
            z = dot(v1, v1, 1) + dot(v2, v2, 1) - 2*dot(v1, v2, 1); % Squared Euclidean distance
           % z = sqrt(z);
    end
    i = (1:n - k)';
    j = i + k;
    D(sub2ind([n n], i, j)) = z;
    D(sub2ind([n n], j, i)) = z;
end
% 'Juan Felipe Restrepo <jrestrepo@bioingenieria.edu.ar>'
