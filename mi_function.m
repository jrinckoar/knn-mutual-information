function MIF = mi_function(x, y, tmax, nn, dt)
%KNN_MI Calculate the mutual information between systems X and Y using the
% algorithm of Kraskov-Grassberger shown in Estimating Mutual Information
% 2004.
%
% Syntax: [I, tyx, txy] = knn-mi(x, y, tmax, nn, dt)
%
% Inputs:
%   x -- Time series from system X
%   y -- Time series from system Y
%   tmax -- Maximum time lag
%   nn -- Number of nearest neighbors
%   dt -- Distance type: 0 inf-norm, 1 squared euclidean-norm
%
% Outputs:
%   MIF -- Mutual information function
%
% $Date: 2019-04-09
%
% $Author: Juan F. Restrepo, Ph.D.
%
% Copyright (c) 2019, Juan F. Restrepo
% All rights reserved.


% Normalize time series to zero mean and unitary standard deviation
y = (y - mean(y)) ./ std(y);
x = (x - mean(x)) ./ std(x);

% Add small amplitude random noise (suggested by Kraskov and Grassberger)
x = x + randn(size(x)) * 10E-10;
y = y + randn(size(y)) * 10E-10;

tau = 0:tmax;
T = length(tau);
MIF = zeros(T, 1);
parfor i = 1:T
    MIF(i) = knn_mi(x(1:end-tau(i)), y(1+tau(i):end) , nn, dt);
end
